﻿using POpusCodec;
using POpusCodec.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

namespace FTF.VoIP
{
    /// <summary>
    /// 입력된 ip, port로 접속하여 서버에서 데이터를 전송받는다.
    /// 전송받은 데이터를 AudioStreamPlayer를 통해서 디바이스에 송출한다.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(VoiceSource))]
    [AddComponentMenu("Voice/Voice Client")]
    public class VoiceClient : MonoBehaviour
    {
        static int EXPECTED_USER_COUNT = 10;
        static Dictionary<int, PrimitiveArrayPool<byte>> pools = new Dictionary<int, PrimitiveArrayPool<byte>>();

        /// <summary>
        /// 예상되는 유저 수(VoiceSource 갯수)를 입력하여 초기화 해 준다. 
        /// 불필요한 GC 생성을 막기위한 것임. 하지만 초반 배열생성에 많은 메모리를 소모할 수 있음.
        /// 기본값인 10명인 경우 40kb의 메모리를 소모할것으로 예상됨.
        /// </summary>
        /// <param name="count"></param>
        public static void InitExpectedUserCount(int count)
        {
            EXPECTED_USER_COUNT = count;
            Debug.LogFormat("Initialize VoiceSource Expected user Count {0}. (expected used Memory {1:n}bytes}", count, (80 * 50) * count); // 평균 80byte의 배열 / 평균 50개의 배열 바리에이션
        }
        static byte[] TakeBytes(int size)
        {
            PrimitiveArrayPool<byte> pool;
            if (!pools.TryGetValue(size, out pool))
            {
                pools.Add(size, pool = new PrimitiveArrayPool<byte>(10, string.Empty, size));
                return new byte[size];
            }
            return pool.AcquireOrCreate();
        }
        static void Release(byte[] data)
        {
            PrimitiveArrayPool<byte> pool;
            if (pools.TryGetValue(data.Length, out pool))
            {
                pool.Release(data);
            }
            else
            {
                pool.Release(data);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Member Variables
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        //-- Inspector
        [Tooltip("접속할 서버 아이피 입니다."), SerializeField, Delayed]
        private string _sourceIP;
        [Tooltip("접속할 서버 포트 입니다."), SerializeField]
        private int _sourcePort = 55155;
        [Space]
        [SerializeField]
        private int _bufferSize = 4096;

        //-- Network
        private bool _isRun;
        private Socket socket;
        private Thread _readThread;

        //-- AudioPlayer
        private VoiceSource voice;

        int _frequency; // 전송될 음성 주사율
        int _channels; // 전송될 음성 체널 수
        int _frameSamplesPerChannel;


        //-- public Properties
        public bool IsConnected { get; private set; }
        public string SourceIP { get { return _sourceIP; } set { if (_sourceIP != value) { _sourceIP = value; clearSocket(); } } }
        public int SourcePort { get { return _sourcePort; } set { if (_sourcePort != value) { _sourcePort = value; clearSocket(); } } }



        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Unity Lifecycles
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        private void Awake()
        {
            voice = GetComponent<VoiceSource>();
        }

        private void OnEnable()
        {
            if (!_isRun)
            {
                _isRun = true;

                _readThread = new Thread(VoiceReadThread);
                _readThread.Name = "VoiceSource Reader";
                _readThread.Start();
            }
        }

        private void OnDisable()
        {
            if (_isRun)
            {
                _isRun = false;
                IsConnected = false;
                clearSocket();

                if (_readThread != null)
                {
                    _readThread.Join();
                    _readThread = null;
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Read Threads
        //
        //////////////////////////////////////////////////////////////////////////////////////////

        private void clearSocket()
        {
            try
            { socket.Close(); }
            catch { }
            socket = null;
        }

        /// <summary>
        /// 대상 서버에 접속한다.
        /// 하지만 접속에 문제가 생기거나, 종료될 경우 재접속하기 위해서
        /// VoiceSource가 Disable될 때 까지, 지속적으로 루프한다.
        /// </summary>
        private void VoiceReadThread()
        {
            using (socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                bool beforeConnected = false;
                while (_isRun)
                {
                    IsConnected = false;
                    try
                    {
                        // 아직 정보가 입력되지 않았음.
                        // 7글자 미만면 ip주소가 아님 (0.0.0.0 : len(7))
                        if (string.IsNullOrEmpty(_sourceIP) && _sourceIP.Length < 7)
                        {
                            Thread.Sleep(100);
                            continue;
                        }

                        // 타겟 서버에 접속시킨다 
                        //Debug.LogFormat("[Voice-Src] Try Connecting {0}:{1}", _sourceIP, _sourcePort);
                        IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(_sourceIP), _sourcePort);
                        socket.Connect(endPoint);

                        // 실제로 접속한 아이피 주소
                        IsConnected = true;
                        beforeConnected = true;
                        Debug.LogFormat("[Voice-Src] Connected {0}:{1}", _sourceIP, _sourcePort);

                        // 읽기 시작
                        ReadSocketRunnable();
                    }
                    catch (EndOfStreamException) { }
                    catch (Exception e)
                    {
                        if (_isRun && beforeConnected && socket != null)
                            Debug.LogException(e);
                    }
                    Thread.Sleep(10);
                }
            }
        }
        /// <summary>
        /// 접속한 서버로부터 기본패킷을 계속 읽어온다.
        /// 2바이트짜리 길이 패킷을 토대로, 데이터를 읽어서 HandlePakcet으로 정보를 인계한다.
        /// </summary>
        void ReadSocketRunnable()
        {
            byte[] receiveBuffer = new byte[_bufferSize];
            using (var receiveStream = new MemoryStream(receiveBuffer, false))
            using (var reader = new BinaryReader(receiveStream))
            using (var originReader = new BinaryReader(new NetworkStream(socket), Encoding.UTF8))
            {
                while (_isRun)
                {
                    int packsize = originReader.ReadUInt16();
                    int offset = 0;

                    while (packsize > offset)
                    {
                        int read = packsize - offset;

                        read = originReader.Read(receiveBuffer, offset, read);
                        offset += read;
                    }
                    receiveStream.Position = 0;

                    AddSendMonitor(packsize + 2);
                    HandlePacket(reader);
                }
            }
        }
        /// <summary>
        /// 전달받은 reader를 토대로 데이터를 처리한다.
        /// </summary>
        void HandlePacket(BinaryReader reader)
        {
            byte opcode = reader.ReadByte();
            switch (opcode)
            {
                case VoiceOpcode.OPCODE_VOICE_INFO:
                    {
                        voice.InitAudioPlayer = false;
                        voice.Frequency = reader.ReadInt32();
                        voice.Channel = reader.ReadByte();
                        voice.FrameSamplesPerChannel = reader.ReadInt32();

                        while (!voice.InitAudioPlayer)
                        {
                            Thread.Sleep(10);
                        }
                    }
                    break;

                case VoiceOpcode.OPCODE_VOICE_DATA:
                    {
                        // 음성 데이터 길이
                        ushort size = reader.ReadUInt16();

                        // 음성 데이터 읽기
                        byte[] pcmBuff = TakeBytes(size);

                        reader.Read(pcmBuff, 0, size);
                        voice.OnAudioBuffer(pcmBuff);

                        Release(pcmBuff);
                    }
                    break;
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Editor Network Monitor
        //
        //////////////////////////////////////////////////////////////////////////////////////////
#if UNITY_EDITOR
        [Space]
        [Header("Network Monitor")]
        public int _totalPacketSize = 0;
        public int _averagePacketSize = 0;
        public DateTime _packetStartTime;
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        void AddSendMonitor(int size)
        {
            if (_packetStartTime.Ticks == 0)
                _packetStartTime = DateTime.Now;

            _totalPacketSize += size + 2;
            _averagePacketSize = (int)(_totalPacketSize / (DateTime.Now - _packetStartTime).TotalSeconds);
        }
#else
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        void AddSendMonitor(int size) { }
#endif
    }
}