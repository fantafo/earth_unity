﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#if ENABLE_SEVENT
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UObject = UnityEngine.Object;

[Serializable]
public class SDelegateParameter
{
    //public enum ParamMode : byte
    //{
    //    Object,
    //    Reflection,
    //    Direct
    //}

    public bool useReflection;
    public UObject obj;
    public string path;

    // programmicaly
    public Type paramType;
    private int _isCachedD;
    private bool _isCached
    {
        get
        {
            if (_isCachedD != 0) Debug.Log(_isCachedD);
            return (_isCachedD != 0 ? ++_isCachedD : 0) > 0;
        }
        set
        {
            if (!value) _isCachedD = 0;
            else _isCachedD++;
        }
    }
    private object _value;

    private MethodInfo methodInfo;
    private PropertyInfo propertyInfo;
    private FieldInfo fieldInfo;

    public SDelegateParameter() { }
    public SDelegateParameter(object value) { Set(value); }
    public SDelegateParameter(UObject obj, string path) { Set(obj, path); }
    public SDelegateParameter(Type type, string path) { Set(type, path); }


    public void Set(object value) { Value = value; }
    public void Set(UObject obj, string path) { this.obj = obj; this.path = path; useReflection = true; }
    public void Set(Type type, string path) { this.paramType = type; this.path = path; useReflection = false; }


    public object Value
    {
        get
        {
            if (_value != null) return _value;

            Prepare();
            if (!useReflection)
            {
                if (paramType == typeof(string)) _value = path;
                else if (paramType == typeof(float)) _value = path.ToFloat(0);
                else if (paramType == typeof(int)) _value = path.ToInt(0);
                else if (paramType == typeof(bool)) _value = path.ToBool(false);
                else if (paramType.IsEnum) _value = Enum.ToObject(paramType, path.ToInt(0));
                else if (paramType == typeof(long)) _value = path.ToLong(0);
                else if (paramType == typeof(short)) _value = path.ToShort(0);
                else if (paramType == typeof(byte)) _value = path.ToByte(0);
                else if (paramType == typeof(double)) _value = path.ToDouble(0);
                else if (paramType == typeof(uint)) _value = path.ToUint(0);
                else if (paramType == typeof(ulong)) _value = path.ToULong(0);
                else if (paramType == typeof(ushort)) _value = path.ToUShort(0);
                else if (paramType == typeof(sbyte)) _value = path.ToSbyte(0);
                else if (paramType == typeof(decimal)) _value = path.ToDecimal(0);
                else if (paramType == typeof(char))
                {
                    if (path == null || path.Length == 0) _value = null;
                    else _value = path[0];
                }
                else if (paramType == typeof(Vector2)) try { _value = JsonUtility.FromJson<Vector2>(path); } catch { _value = Vector2.zero; }
                else if (paramType == typeof(Vector3)) try { _value = JsonUtility.FromJson<Vector3>(path); } catch { _value = Vector3.zero; }
                else if (paramType == typeof(Vector4)) try { _value = JsonUtility.FromJson<Vector4>(path); } catch { _value = Vector4.zero; }
                else if (paramType == typeof(Quaternion)) try { _value = JsonUtility.FromJson<Quaternion>(path); } catch { _value = Quaternion.identity; }
                return _value;
            }
            else
            {
                if (fieldInfo != null) return fieldInfo.GetValue(obj);
                if (propertyInfo != null) return propertyInfo.GetValue(obj, null);
                if (methodInfo != null) return methodInfo.Invoke(obj, null);
#if !NETFX_CORE
                if (paramType != null && paramType.IsValueType) return null;
#endif
                return Convert.ChangeType(null, paramType);
            }
        }
        set
        {
            _value = value;
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                if (value == null) path = "";
                else if (paramType.IsEnum) path = value.GetHashCode().ToString();
                else if (paramType == typeof(Vector2)) path = JsonUtility.ToJson(value);
                else if (paramType == typeof(Vector3)) path = JsonUtility.ToJson(value);
                else if (paramType == typeof(Vector4)) path = JsonUtility.ToJson(value);
                else if (paramType == typeof(Quaternion)) path = JsonUtility.ToJson(value);
                else if (paramType == typeof(string)) path = (string)value;
                else path = value.ToString();
                _value = null;
            }
#endif
        }
    }

    public void Reload()
    {
        fieldInfo = null;
        propertyInfo = null;
        methodInfo = null;
    }

    public void Prepare()
    {
        if (!useReflection
            || fieldInfo != null
            || propertyInfo != null
            || methodInfo != null) return;
          
        if (obj != null && !string.IsNullOrEmpty(path))
        {
            Type type = obj.GetType();
#if NETFX_CORE
            fieldInfo = type.GetRuntimeField(path);
			if (fieldInfo == null) propInfo = type.GetRuntimeProperty(path);
			if (propInfo == null) methodInfo = type.GetRuntimeMethod(path);
#else
            fieldInfo = type.GetField(path);
            if (fieldInfo == null) propertyInfo = type.GetProperty(path);
            if (propertyInfo == null) methodInfo = type.GetMethod(path);
#endif
        }
    }

    public override string ToString()
    {
        if (useReflection)
        {
            if (obj != null && !string.IsNullOrEmpty(path))
            {
                Prepare();
                string name = SDelegate.ConvertObjectName(obj) + "/";

                if (fieldInfo != null) name += ConvertName(fieldInfo);
                else if (propertyInfo != null) name += ConvertName(propertyInfo);
                else if (methodInfo != null) name += ConvertName(methodInfo);
                else name += path + " (Missing)";
                return name;
            }
            else
            {
                return "None";
            }
        }
        else
        {
            return "SDelegateParameter : " + path;
        }
    }

    public static string ConvertName(MethodInfo m)
    {
        return "(M) " + m.Name;
    }

    public static string ConvertName(PropertyInfo p)
    {
        return "(P) " + p.Name;
    }

    public static string ConvertName(FieldInfo f)
    {
        return f.Name;
    }

    public static string ConvertTypeName(Type type)
    {
        if (type == typeof(string)) return "string";
        else if (type.IsValueType)
        {
            if (type == typeof(byte)) return "byte";
            else if (type == typeof(sbyte)) return "sbyte";
            else if (type == typeof(short)) return "short";
            else if (type == typeof(ushort)) return "ushort";
            else if (type == typeof(int)) return "int";
            else if (type == typeof(uint)) return "uint";
            else if (type == typeof(long)) return "long";
            else if (type == typeof(ulong)) return "ulong";
            else if (type == typeof(bool)) return "bool";
            else if (type == typeof(float)) return "float";
            else if (type == typeof(double)) return "double";
            else if (type == typeof(decimal)) return "decimal";
            else if (type == typeof(char)) return "char";
        }
        return type.Name;
    }
}
#endif