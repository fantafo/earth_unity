﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;

public static class GameObjectExtension
{
    public static T GetComponentOrAdd<T>(this GameObject go) where T : Component
    {
        var obj = go.GetComponent<T>();
        if (!obj)
        {
            return go.AddComponent<T>();
        }
        return obj;
    }
    public static T GetComponentOrAdd<T>(this Component comp) where T : Component
    {
        var obj = comp.GetComponent<T>();
        if (!obj)
        {
            return comp.gameObject.AddComponent<T>();
        }
        return obj;
    }

    public static void SetActive(this GameObject[] objs, bool active)
    {
        if (objs != null)
        {
            foreach (var obj in objs)
            {
                if (obj != null)
                {
                    obj.SetActive(active);
                }
            }
        }
    }

    public static void SetGameObectActive(this Component[] objs, bool active)
    {
        if (objs != null)
        {
            foreach (var obj in objs)
            {
                if (obj != null)
                {
                    obj.gameObject.SetActive(active);
                }
            }
        }
    }

    public static void SetActiveChildren(this GameObject objs, bool active)
    {
        Transform trans = objs.transform;
        for (int i = 0, len = trans.childCount; i < len; i++)
        {
            trans.GetChild(i).gameObject.SetActive(active);
        }
    }

    /// <summary>
    /// Instantiate 확장
    /// </summary>
    /// <param name="p_prefab">타겟 프리팹</param>
    /// <param name="p_parent">부모 게임오브젝트</param>
    /// <param name="p_position">Position</param>
    /// <param name="p_rotation">Roatation</param>
    /// <param name="p_name">게임오브젝트 이름</param>
    /// <returns></returns>
    public static GameObject InstantiateEx(this GameObject target,
        GameObject original,
        GameObject p_parent,
        Vector3 p_position,
        Quaternion p_rotation,
        string p_name = null)
    {
        GameObject __result = GameObject.Instantiate(original);
        if (p_parent)
        {
            __result.transform.SetParent(p_parent.transform);
            __result.transform.localPosition = p_position;
            __result.transform.localRotation = p_rotation;
            __result.transform.localScale = Vector3.one;
        }
        else
        {
            __result.transform.position = p_position;
            __result.transform.rotation = p_rotation;
        }
        if (p_name != null)
        {
            __result.name = p_name;
        }
        return __result;
    }

    public static void Destroy(this GameObject target)
    {
#if UNITY_EDITOR
        if (!Application.isPlaying)
        {
            GameObject.DestroyImmediate(target);
        }
        else
#endif
        {
            GameObject.Destroy(target);
        }
    }
}
