﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenLightExtension
{
    private static readonly Action<Light, float> _changeLightIntensity = __changeLightIntensity;
    private static void __changeLightIntensity(Light light, float val) { light.intensity = val; }
    public static STweenState twnIntensity(this Light light, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Light>, Light, float>(light, light.intensity, to, duration, _changeLightIntensity);
    }
    public static STweenState twnIntensity(this Light light, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Light>, Light, float>(light, from, to, duration, _changeLightIntensity);
    }


    private static readonly Action<Light, float> _changeColorAlpha = __changeColorAlpha;
    private static void __changeColorAlpha(Light light, float val) { light.color = light.color.SetAlpha(val); }
    public static STweenState twnColorAlpha(this Light light, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Light>, Light, float>(light, light.intensity, to, duration, _changeColorAlpha);
    }
    public static STweenState twnColorAlpha(this Light light, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Light>, Light, float>(light, from, to, duration, _changeColorAlpha);
    }


    private static readonly Action<Light, Color> _changeColor = __changeColor;
    private static void __changeColor(Light light, Color val) { light.color = val; }
    public static STweenState twnColor(this Light light, Color to, float duration)
    {
        return STween.Play<ActionColorObject<Light>, Light, Color>(light, light.color, to, duration, _changeColor);
    }
    public static STweenState twnColor(this Light light, Color from, Color to, float duration)
    {
        return STween.Play<ActionColorObject<Light>, Light, Color>(light, from, to, duration, _changeColor);
    }


    private static readonly Action<Light, float> _changeShadowStrength = __changeShadowStrength;
    private static void __changeShadowStrength(Light light, float val) { light.shadowStrength = val; }
    public static STweenState twnShadowStrength(this Light light, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Light>, Light, float>(light, light.shadowStrength, to, duration, _changeShadowStrength);
    }
    public static STweenState twnShadowStrength(this Light light, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Light>, Light, float>(light, from, to, duration, _changeShadowStrength);
    }


}