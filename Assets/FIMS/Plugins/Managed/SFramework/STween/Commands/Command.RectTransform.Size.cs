﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using UnityEngine;
using SFramework.TweenAction;

public partial class STween
{
    #region only to
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                            Size                             //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState size(Component comp, Vector2 to, float duration)
    {
        return size((comp != null ? comp.transform : null) as RectTransform, to, duration);
    }
    public static STweenState size(GameObject go, Vector2 to, float duration)
    {
        return size((go != null ? go.transform : null) as RectTransform, to, duration);
    }
    public static STweenState size(Transform trans, Vector2 to, float duration)
    {
        return size(trans as RectTransform, to, duration);
    }
    public static STweenState size(RectTransform trans, Vector2 to, float duration)
    {
        return Play<ActionSize, RectTransform, Vector2>(trans, to, duration);
    }



    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                         Size Single                         //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState sizeX(Component comp, float to, float duration)
    {
        return sizeX((comp != null ? comp.transform : null) as RectTransform, to, duration);
    }
    public static STweenState sizeX(GameObject go, float to, float duration)
    {
        return sizeX((go != null ? go.transform : null) as RectTransform, to, duration);
    }
    public static STweenState sizeX(Transform trans, float to, float duration)
    {
        return sizeX(trans as RectTransform, to, duration);
    }
    public static STweenState sizeX(RectTransform trans, float to, float duration)
    {
        return Play<ActionSizeX, RectTransform, float>(trans, to, duration);
    }

    public static STweenState sizeY(Component comp, float to, float duration)
    {
        return sizeY((comp != null ? comp.transform : null) as RectTransform, to, duration);
    }
    public static STweenState sizeY(GameObject go, float to, float duration)
    {
        return sizeY((go != null ? go.transform : null) as RectTransform, to, duration);
    }
    public static STweenState sizeY(Transform trans, float to, float duration)
    {
        return sizeY(trans as RectTransform, to, duration);
    }
    public static STweenState sizeY(RectTransform trans, float to, float duration)
    {
        return Play<ActionSizeY, RectTransform, float>(trans, to, duration);
    }
    #endregion

    #region from, to
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                            Size                             //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState size(Component comp, Vector2 from, Vector2 to, float duration)
    {
        return size((comp != null ? comp.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState size(GameObject go, Vector2 from, Vector2 to, float duration)
    {
        return size((go != null ? go.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState size(Transform trans, Vector2 from, Vector2 to, float duration)
    {
        return size(trans as RectTransform, from, to, duration);
    }
    public static STweenState size(RectTransform trans, Vector2 from, Vector2 to, float duration)
    {
        return Play<ActionSize, RectTransform, Vector2>(trans, from, to, duration);
    }



    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                         Size Single                         //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState sizeX(Component comp, float from, float to, float duration)
    {
        return sizeX((comp != null ? comp.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState sizeX(GameObject go, float from, float to, float duration)
    {
        return sizeX((go != null ? go.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState sizeX(Transform trans, float from, float to, float duration)
    {
        return sizeX(trans as RectTransform, from, to, duration);
    }
    public static STweenState sizeX(RectTransform trans, float from, float to, float duration)
    {
        return Play<ActionSizeX, RectTransform, float>(trans, from, to, duration);
    }

    public static STweenState sizeY(Component comp, float from, float to, float duration)
    {
        return sizeY((comp != null ? comp.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState sizeY(GameObject go, float from, float to, float duration)
    {
        return sizeY((go != null ? go.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState sizeY(Transform trans, float from, float to, float duration)
    {
        return sizeY(trans as RectTransform, from, to, duration);
    }
    public static STweenState sizeY(RectTransform trans, float from, float to, float duration)
    {
        return Play<ActionSizeY, RectTransform, float>(trans, from, to, duration);
    }
    #endregion
}
#endif