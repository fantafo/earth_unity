﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using UnityEngine;
using SFramework.TweenAction;

public partial class STween
{
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                          Value                              //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState valueInt<T>(T obj, int from, int to, int duration, Action<T, int> action) where T : class
    {
        return Play<ActionIntObject<T>, T, int>(obj, from, to, duration, action);
    }
    public static STweenState value<T>(T obj, float from, float to, float duration, Action<T, float> action) where T : class
    {
        return Play<ActionFloatObject<T>, T, float>(obj, from, to, duration, action);
    }
    public static STweenState value<T>(T obj, Vector2 from, Vector2 to, float duration, Action<T, Vector2> action) where T : class
    {
        return Play<ActionVector2Object<T>, T, Vector2>(obj, from, to, duration, action);
    }
    public static STweenState value<T>(T obj, Vector3 from, Vector3 to, float duration, Action<T, Vector3> action) where T : class
    {
        return Play<ActionVector3Object<T>, T, Vector3>(obj, from, to, duration, action);
    }
    public static STweenState value<T>(T obj, Vector4 from, Vector4 to, float duration, Action<T, Vector4> action) where T : class
    {
        return Play<ActionVector4Object<T>, T, Vector4>(obj, from, to, duration, action);
    }
    public static STweenState value<T>(T obj, Quaternion from, Quaternion to, float duration, Action<T, Quaternion> action) where T : class
    {
        return Play<ActionQuaternionObject<T>, T, Quaternion>(obj, from, to, duration, action);
    }
    public static STweenState value<T>(T obj, Color from, Color to, float duration, Action<T, Color> action) where T : class
    {
        return Play<ActionColorObject<T>, T, Color>(obj, from, to, duration, action);
    }
    public static STweenState value<T>(T obj, Bounds from, Bounds to, float duration, Action<T, Bounds> action) where T : class
    {
        return Play<ActionBoundsObject<T>, T, Bounds>(obj, from, to, duration, action);
    }
    public static STweenState value<T>(T obj, Rect from, Rect to, float duration, Action<T, Rect> action) where T : class
    {
        return Play<ActionRectObject<T>, T, Rect>(obj, from, to, duration, action);
    }
    public static STweenState value<T>(T obj, RectOffset from, RectOffset to, float duration, Action<T, RectOffset> action) where T : class
    {
        return Play<ActionRectOffsetObject<T>, T, RectOffset>(obj, from, to, duration, action);
    }



    public static STweenState valueInt<T>(T obj, int to, float duration, Action<T, int> action) where T : class
    {
        return Play<ActionIntObject<T>, T, int>(obj, 0, to, duration, action);
    }
    public static STweenState value<T>(T obj, float to, float duration, Action<T, float> action) where T : class
    {
        return Play<ActionFloatObject<T>, T, float>(obj, 0, to, duration, action);
    }
    public static STweenState value<T>(T obj, Vector2 to, float duration, Action<T, Vector2> action) where T : class
    {
        return Play<ActionVector2Object<T>, T, Vector2>(obj, Vector2.zero, to, duration, action);
    }
    public static STweenState value<T>(T obj, Vector3 to, float duration, Action<T, Vector3> action) where T : class
    {
        return Play<ActionVector3Object<T>, T, Vector3>(obj, Vector3.zero, to, duration, action);
    }
    public static STweenState value<T>(T obj, Vector4 to, float duration, Action<T, Vector4> action) where T : class
    {
        return Play<ActionVector4Object<T>, T, Vector4>(obj, Vector4.zero, to, duration, action);
    }
    public static STweenState value<T>(T obj, Quaternion to, float duration, Action<T, Quaternion> action) where T : class
    {
        return Play<ActionQuaternionObject<T>, T, Quaternion>(obj, Quaternion.identity, to, duration, action);
    }
    public static STweenState value<T>(T obj, Color to, float duration, Action<T, Color> action) where T : class
    {
        return Play<ActionColorObject<T>, T, Color>(obj, new Color(0, 0, 0, 0), to, duration, action);
    }
    public static STweenState value<T>(T obj, Bounds to, float duration, Action<T, Bounds> action) where T : class
    {
        return Play<ActionBoundsObject<T>, T, Bounds>(obj, new Bounds(Vector3.zero, Vector3.zero), to, duration, action);
    }
    public static STweenState value<T>(T obj, Rect to, float duration, Action<T, Rect> action) where T : class
    {
        return Play<ActionRectObject<T>, T, Rect>(obj, new Rect(0, 0, 0, 0), to, duration, action);
    }
    public static STweenState value<T>(T obj, RectOffset to, float duration, Action<T, RectOffset> action) where T : class
    {
        return Play<ActionRectOffsetObject<T>, T, RectOffset>(obj, new RectOffset(0, 0, 0, 0), to, duration, action);
    }
}
