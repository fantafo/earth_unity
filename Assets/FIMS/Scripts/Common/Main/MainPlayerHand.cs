﻿using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 플레이어가 사용하고있는 현재 손을 정의한다.
    /// 또한 손 모델을 끄고 키는 기능을 가지고있다.
    /// </summary>
    public class MainPlayerHand : SMonoBehaviour
    {
        public GameObject model;
        bool visible;

        private void OnEnable()
        {
            MainPlayerController.main.hand = this;
            visible = model.activeSelf;
            Update();
        }
        private void OnDisable()
        {
            MainPlayerController.main.hand = this;
        }

        private void Update()
        {
            if(MainPlayerController.main != null && MainPlayerController.main.UseControllerModel != visible)
            {
                model.gameObject.SetActive((visible = !visible));
            }
        }
    }
}