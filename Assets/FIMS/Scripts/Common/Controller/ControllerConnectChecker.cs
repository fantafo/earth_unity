﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;
using UnityEngine.EventSystems;

namespace FTF
{
    /// <summary>
    /// 컨트롤러 연결상태에 따라서 대상 오브젝트의 active 상태를 결정한다.
    /// </summary>
    public class ControllerConnectChecker : SMonoBehaviour
    {
        public bool inverse;
        public GameObject target;
        bool isPresentController;

        private void Awake()
        {
            isPresentController = !VRInput.isPresentController;
            Update();
        }

        private void Update()
        {
            if (isPresentController != VRInput.isPresentController)
            {
                isPresentController = VRInput.isPresentController;

                // 연결됨
                if (isPresentController)
                {
                    target.SetActive(!inverse);
                }
                // 연결해제됨
                else
                {
                    target.SetActive(inverse);
                }
            }
        }
    }
}