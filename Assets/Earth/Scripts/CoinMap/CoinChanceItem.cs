﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
using UnityEngine.EventSystems;

/// <summary>
/// 코인맵에서 사용하는 코인 아이템입니다.
/// </summary>
public class CoinChanceItem : SMonoBehaviour, IPointerClickHandler
{
    internal int idx;
    public GameObject coin;
    public GameObject effect;

    private void Reset()
    {
        coin = GetComponentInChildren<Renderer>().gameObject;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        GetComponentInParent<CoinChance>().BroadcastCatch(this);
    }

    public void OnClick()
    {
        GetComponentInParent<CoinChance>().BroadcastCatch(this);
    }
}
